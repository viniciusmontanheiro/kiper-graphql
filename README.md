# Kiper GraphQL

This project is the first step to start with GraphQL at Segware

Up-to-date documentation and explanations for Apollo Server can be found on docs.apollostack.com

## Getting started

```sh
cd kiper-graphql
yarn
```
## Running development mode

```sh
yarn dev
```

Then open [http://localhost:3000/playground](http://localhost:3000/graphql)

