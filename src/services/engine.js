import { Engine } from 'apollo-engine';

const engine = new Engine({
  engineConfig: {
    apiKey: 'service:vcrzy:nLh0HDK4L62klIn9sqCaRg',
  },
  graphqlPort: process.env.PORT || 3000,
  endpoint: '/graphql',
});
engine.start();

export default engine;
