const getContext = req => {
  const { body: { query } } = req;
  const token = req.headers.authorization;
};

export default getContext;
