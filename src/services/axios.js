import axios from 'axios';
import Raven from 'raven';
import { notAuthorized, customError } from './error';

export const setupErrorInterceptor = () => {
  axios.interceptors.response.use(
    response => response,
    error => {
      const { response } = error;
      if (response && response.status === 401) {
        return Promise.reject(notAuthorized());
      }
      if (response && response.status === 400) {
        return Promise.reject(customError({ data: response.data }));
      }
      Raven.captureException(error);
      return Promise.reject(error);
    }
  );
};
