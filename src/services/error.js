import { GraphQLError } from 'graphql/error';

export const notAuthorized = () =>
  new GraphQLError('not_authorized');

export const customError = ({ data: { response: { key, detail } } }) => {
  return new GraphQLError({
     message: key || 'Error',
     detail: detail || 'Detail undefined from server',
     code: 400,
   });
};
