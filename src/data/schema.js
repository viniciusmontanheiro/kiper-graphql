import { makeExecutableSchema } from 'graphql-tools';
import resolvers from './resolvers';

const typeDefs = ` 

  type Timeline {
    id: ID!
    description: String!
    active: Boolean    
  }

  type Query {
    fetchTimeline(accessId: String!): [Timeline!]
  }
`;

const schema = makeExecutableSchema({ typeDefs, resolvers });

export default schema;
