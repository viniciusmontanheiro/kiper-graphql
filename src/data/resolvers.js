import { Viewer } from './connectors';

const resolvers = {
  Query: {
    fetchTimeline: (obj, args, context) => {
      const { token, accessId } = context;
      return Viewer.fetchTimeline({ token, accessId });
    },
  },
};

export default resolvers;
