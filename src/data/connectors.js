import { fetchTimeline } from '../kiper/mobile/queries';

const Mutation = {

};

const Viewer = {
  fetchTimeline({ token, accessId }) {
    return fetchTimeline({ token, accessId });
  },
};

export { Mutation, Viewer };
