import express from 'express';
import { graphqlExpress } from 'apollo-server-express';
import expressPlayground from 'graphql-playground-middleware-express';
import engine from './services/engine';
import { setupErrorInterceptor } from './services/axios';
import bodyParser from 'body-parser';
import schema from './data/schema';
import getContext from './services/context';

if (!global._babelPolyfill) {
  require('babel-polyfill');
}

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
const GRAPHQL_PORT = 3000;
const app = express();
setupErrorInterceptor();
app.use(engine.expressMiddleware());
app.use(bodyParser.json());
app.use('/graphql', (next) => next());
app.use(
  '/graphql',
  graphqlExpress(req => ({
    schema,
    context: getContext(req),
    tracing: true,
    cacheControl: true,
  }))
);
app.get('/playground', expressPlayground({ endpoint: '/graphql' }));
app.get('/health', (req, res) => {
  res.status(200).json({ status: 'UP' });
});

app.listen(GRAPHQL_PORT, () =>
  console.log(
    `GraphiQL is now running on http://localhost:${GRAPHQL_PORT}/playground`
  )
);
